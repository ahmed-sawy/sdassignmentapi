﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SDAssignment.Domain.Models
{
    public class ItemModel
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemTitle { get; set; }
        public string ItemDesc { get; set; }
        public int StepId { get; set; }
    }
}
