﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SDAssignment.Domain.Models
{
    public class StepModel
    {
        public int StepId { get; set; }
        public string StepName { get; set; }
        public string StepDesc { get; set; }

        public bool IsSelected { get; set; }

        public List<ItemModel> Items { get; set; }

    }
}
