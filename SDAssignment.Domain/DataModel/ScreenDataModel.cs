﻿using SDAssignment.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SDAssignment.Domain.DataModel
{
    public class ScreenDataModel
    {
        public List<StepModel> Steps { get; set; }
        public ItemModel Item { get; set; }
        public StepModel Step { get; set; }

    }
}
