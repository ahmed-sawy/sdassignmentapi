﻿using SDAssignment.Domain.DataModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace SDAssignment.Services.Interfaces
{
    public interface IScreenService
    {
        ScreenDataModel GetStepsWithItems();
        ScreenDataModel AddNewStep(ScreenDataModel screenData);
        ScreenDataModel AddNewItemAtSelectedStep(ScreenDataModel screenData);
        ScreenDataModel RemoveSelectedStepWithItems(int stepId);
        ScreenDataModel RemoveItem(int itemId);

    }
}
