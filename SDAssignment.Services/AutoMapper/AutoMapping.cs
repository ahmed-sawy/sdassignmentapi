﻿using AutoMapper;
using SDAssignment.Data.Entities;
using SDAssignment.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SDAssignment.Services.AutoMapper
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<Item, ItemModel>().ReverseMap();
            CreateMap<Step, StepModel>().ReverseMap();
        }
    }
}
