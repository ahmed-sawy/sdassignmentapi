﻿using AutoMapper;
using SDAssignment.Data.Entities;
using SDAssignment.Data.UnitOfWorks.Interfaces;
using SDAssignment.Domain.DataModel;
using SDAssignment.Domain.Models;
using SDAssignment.Services.Interfaces;
using System;
using System.Collections.Generic;

namespace SDAssignment.Services.Classes
{
    public class ScreenService : IScreenService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public ScreenService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public ScreenDataModel AddNewItemAtSelectedStep(ScreenDataModel screenData)
        {
            if (screenData.Item.ItemId > 0)
            {
                this.unitOfWork.Items.Update(this.mapper.Map<Item>(screenData.Item));
            }
            else
            {
                screenData.Item.StepId = screenData.Step.StepId;
                this.unitOfWork.Items.Add(this.mapper.Map<Item>(screenData.Item));
            }

            try
            {
                this.unitOfWork.Complete();
                return this.GetStepsWithItems();
            }
            catch(Exception ex)
            {
                ex.ToString();
                return null;
            }
        }

        public ScreenDataModel AddNewStep(ScreenDataModel screenData)
        {
            this.unitOfWork.Steps.Add(this.mapper.Map<Step>(screenData.Step));
            try
            {
                this.unitOfWork.Complete();
                return this.GetStepsWithItems();
            }
            catch(Exception ex)
            {
                ex.ToString();
                return null;
            }
        }

        public ScreenDataModel GetStepsWithItems()
        {
            ScreenDataModel screenData = new ScreenDataModel();

            screenData.Steps = this.mapper.Map<List<StepModel>>(this.unitOfWork.Steps.GetStepsWithItems());
            screenData.Item = new ItemModel();
            screenData.Step = new StepModel();

            return screenData;
        }

        public ScreenDataModel RemoveItem(int itemId)
        {
            this.unitOfWork.Items.Remove(this.unitOfWork.Items.GetByID(itemId));

            try
            {
                this.unitOfWork.Complete();
                return this.GetStepsWithItems();
            }
            catch (Exception ex)
            {
                ex.ToString();
                return null;
            }
        }

        public ScreenDataModel RemoveSelectedStepWithItems(int stepId)
        {
            this.unitOfWork.Steps.Remove(this.unitOfWork.Steps.GetByID(stepId));

            try
            {
                this.unitOfWork.Complete();
                return this.GetStepsWithItems();
            }
            catch(Exception ex)
            {
                ex.ToString();
                return null;
            }
        }
    }
}
