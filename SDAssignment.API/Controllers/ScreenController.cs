﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SDAssignment.Domain.DataModel;
using SDAssignment.Domain.Models;
using SDAssignment.Services.Interfaces;

namespace SDAssignment.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ScreenController : ControllerBase
    {
        private readonly IScreenService screenService;

        public ScreenController(IScreenService screenService)
        {
            this.screenService = screenService;
        }

        [HttpGet]
        public ObjectResult GetInitObject()
        {
            ScreenDataModel dataModel = this.screenService.GetStepsWithItems();

            if (dataModel != null)
                return Ok(dataModel);
            else
                return BadRequest(dataModel);
        }

        [HttpPost]
        public ObjectResult AddItem(ScreenDataModel screenData)
        {
            ScreenDataModel result = this.screenService.AddNewItemAtSelectedStep(screenData);
            if (result != null)
                return Ok(result);
            else
                return BadRequest(screenData);
        }

        [HttpPost]
        public ObjectResult AddStep(ScreenDataModel screenData)
        {
            ScreenDataModel result = this.screenService.AddNewStep(screenData);
            if (result != null)
                return Ok(result);
            else
                return BadRequest(screenData);
        }

        [HttpPost]
        public ObjectResult RemoveStep(StepModel step)
        {
            ScreenDataModel result = this.screenService.RemoveSelectedStepWithItems(step.StepId);
            if (result != null)
                return Ok(result);
            else
                return BadRequest(result);
        }

        [HttpPost]
        public ObjectResult RemoveItem(ItemModel item)
        {
            ScreenDataModel result = this.screenService.RemoveItem(item.ItemId);
            if (result != null)
                return Ok(result);
            else
                return BadRequest(result);
        }
    }
}
