using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SDAssignment.Data.DAL;
using SDAssignment.Data.Repositories.Classes;
using SDAssignment.Data.Repositories.Interfaces;
using SDAssignment.Data.UnitOfWorks.Classes;
using SDAssignment.Data.UnitOfWorks.Interfaces;
using SDAssignment.Services.AutoMapper;
using SDAssignment.Services.Classes;
using SDAssignment.Services.Interfaces;

namespace SDAssignment.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<SDAssignmentContext>(
                option => option.UseSqlServer(Configuration.GetConnectionString("DocCareConnection")));

            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapping());
            });
            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);


            services.AddScoped(typeof(DbContext), typeof(SDAssignmentContext));

            services.AddScoped(typeof(IUnitOfWork), typeof(UnitOfWork));


            services.AddScoped(typeof(IScreenService), typeof(ScreenService));


            services.AddScoped(typeof(IItemsRepo), typeof(ItemsRepo));
            services.AddScoped(typeof(IStepsRepo), typeof(StepsRepo));

            // to enable CORS
            services.AddCors(c =>
            {
                c.AddPolicy("AllowAll", options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors("AllowAll");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
