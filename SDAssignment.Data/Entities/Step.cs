﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SDAssignment.Data.Entities
{
    public class Step
    {
        [Required]
        public int StepId { get; set; }
        [MaxLength(200)]
        public string StepName { get; set; }
        [MaxLength(2000)]
        public string StepDesc { get; set; }


        public ICollection<Item> Items { get; set; }
    }
}
