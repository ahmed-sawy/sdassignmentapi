﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SDAssignment.Data.Entities
{
    public class Item
    {
        [Required]
        public int ItemId { get; set; }
        [MaxLength(200)]
        public string ItemName { get; set; }
        [MaxLength(200)]
        public string ItemTitle { get; set; }
        [MaxLength(2000)]
        public string ItemDesc { get; set; }
        [Required]
        public int StepId { get; set; }

        public Step Step { get; set; }
    }
}
