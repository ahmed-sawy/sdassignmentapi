﻿using Microsoft.EntityFrameworkCore;
using SDAssignment.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SDAssignment.Data.DAL
{
    public class SDAssignmentContext : DbContext
    {
        public SDAssignmentContext(DbContextOptions<SDAssignmentContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
        }

        public DbSet<Item> Items { get; set; }
        public DbSet<Step> Steps { get; set; }
    }
}
