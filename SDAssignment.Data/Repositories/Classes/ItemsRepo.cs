﻿using Microsoft.EntityFrameworkCore;
using SDAssignment.Data.DAL;
using SDAssignment.Data.Entities;
using SDAssignment.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SDAssignment.Data.Repositories.Classes
{
    public class ItemsRepo : GenericRepo<Item>, IItemsRepo
    {
        public ItemsRepo(DbContext context) 
            : base(context)
        {
        }

        public SDAssignmentContext context
        {
            get { return context as SDAssignmentContext; }
        }
    }
}
