﻿using Microsoft.EntityFrameworkCore;
using SDAssignment.Data.DAL;
using SDAssignment.Data.Entities;
using SDAssignment.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SDAssignment.Data.Repositories.Classes
{
    public class StepsRepo : GenericRepo<Step>, IStepsRepo
    {
        private readonly DbContext context;
        public StepsRepo(DbContext context)
            : base(context)
        {
            this.context = context;
        }

        public List<Step> GetStepsWithItems()
        {
            List<Step> steps = new List<Step>();
            try
            {
                steps = this.context.Set<Step>()
                    .Include(s => s.Items)
                    .ToList();

                if (steps != null)
                    return steps;
                else
                    return new List<Step>();
            }
            catch(Exception ex)
            {
                ex.ToString();
                return null;
            }
        }
    }
}
