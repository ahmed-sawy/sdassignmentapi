﻿using Microsoft.EntityFrameworkCore;
using SDAssignment.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SDAssignment.Data.Repositories.Classes
{
    public class GenericRepo<Entity> : IGenericRepo<Entity> where Entity : class
    {
        private readonly DbContext context;
        public GenericRepo(DbContext context)
        {
            this.context = context;
        }

        public void Add(Entity oEntity)
        {
            this.context.Set<Entity>().Add(oEntity);
        }

        public void AddRange(List<Entity> lEntities)
        {
            this.context.Set<Entity>().AddRange(lEntities);
        }

        public List<Entity> GetAll()
        {
            return this.context.Set<Entity>().ToList();
        }

        public Entity GetByID(int ID)
        {
            return this.context.Set<Entity>().Find(ID);
        }

        public void Remove(Entity oEntity)
        {
            this.context.Set<Entity>().Remove(oEntity);
        }

        public void RemoveRange(List<Entity> lEntities)
        {
            this.context.Set<Entity>().RemoveRange(lEntities);
        }

        public List<Entity> Search(Expression<Func<Entity, bool>> predicate)
        {
            return this.context.Set<Entity>().Where(predicate).ToList();
        }

        public void Update(Entity oEntity)
        {
            this.context.Entry(oEntity).State = EntityState.Modified;
        }
    }
}
