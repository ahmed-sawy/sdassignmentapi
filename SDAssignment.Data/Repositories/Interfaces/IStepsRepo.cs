﻿using SDAssignment.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SDAssignment.Data.Repositories.Interfaces
{
    public interface IStepsRepo : IGenericRepo<Step>
    {
        List<Step> GetStepsWithItems();
    }
}
