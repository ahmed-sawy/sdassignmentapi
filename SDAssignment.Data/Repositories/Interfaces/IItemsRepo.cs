﻿using SDAssignment.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SDAssignment.Data.Repositories.Interfaces
{
    public interface IItemsRepo : IGenericRepo<Item>
    {
    }
}
