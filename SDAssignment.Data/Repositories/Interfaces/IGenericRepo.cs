﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace SDAssignment.Data.Repositories.Interfaces
{
    public interface IGenericRepo<Entity>
    {
        Entity GetByID(int ID);
        List<Entity> GetAll();

        List<Entity> Search(Expression<Func<Entity, bool>> predicate);

        void Update(Entity oEntity);

        void Add(Entity oEntity);
        void AddRange(List<Entity> lEntities);

        void Remove(Entity oEntity);
        void RemoveRange(List<Entity> lEntities);
    }
}
