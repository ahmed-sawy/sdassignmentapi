﻿using SDAssignment.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SDAssignment.Data.UnitOfWorks.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IItemsRepo Items { get; }
        IStepsRepo Steps { get; }

        int Complete();
    }
}
