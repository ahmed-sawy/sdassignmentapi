﻿using Microsoft.EntityFrameworkCore;
using SDAssignment.Data.Repositories.Interfaces;
using SDAssignment.Data.UnitOfWorks.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SDAssignment.Data.UnitOfWorks.Classes
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext context;

        public UnitOfWork(DbContext context, IItemsRepo Items, IStepsRepo Steps)
        {
            this.context = context;

            this.Items = Items;
            this.Steps = Steps;
        }

        public IItemsRepo Items { get; private set; }
        public IStepsRepo Steps { get; private set; }

        public int Complete()
        {
            return this.context.SaveChanges();
        }

        public void Dispose()
        {
            this.context.Dispose();
        }
    }
}
